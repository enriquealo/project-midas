<?php
class Agreement extends CI_Model
{
    const table = 'agreements';
    const primary_key = 'agreement_id';

    const order_field = 'expiration';
    const default_order = "asc";

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_all_entries()
    {
        $table = self::table;
 
        $query = $this->db->get($table);
        $this->db->order_by(self::order_field,self::order_field);

        if ($query->num_rows() > 0)  
        {  
             return $query->result();
        }
        else
        {  
              return false;
        }
       
    }
    
    function get_entry_by_id($id = null)
    { 
        $table = self::table;
        if($id == null)
        {
            return false;
        }
        else
        {
            $this->db->from($table)->where(self::primary_key,$id)->limit(1);

            $query = $this->db->get();
            if($query->num_rows() > 0)
            {
                return $query->result();
            }   
            else
            {
                return false;
            }
        }
    }
    
    function get_model_name()
    {
        return "Agreement";
    }



    function get_entries_by_business($business_id){
        $table = self::table;
        
        if($business_id == null)
        {
            return false;
        }
        else
        {

            $this->db->from($table);
            $this->db->where('business_id',$business_id);

            $query = $this->db->get();
             
            if($query->num_rows() > 0)
            {
                return $query->result();
            }   
            else
            {
                return false;
            }
        }
    }

    function get_entries_by_university($university_id){
        $table = self::table;
        
        if($university_id == null)
        {
            return false;
        }
        else
        {

            $this->db->from($table);
            $this->db->where('university_id',$university_id);

            $query = $this->db->get();
             
            if($query->num_rows() > 0)
            {
                return $query->result();
            }   
            else
            {
                return false;
            }
        }
    }

    function insert_entry($business_id,$university_id,$expiration)
    {
        $table = self::table;

        $data = array(
           'business_id' => $business_id ,
           'university_id' => $university_id ,
           'expiration' => $expiration 
        );

        $this->db->insert($table,$data);

        
    }

    function update_entry($id,$business_id,$university_id,$expiration)
    {
        $table = self::table;
        if($id == null)
        {
            return false;
        }
        else
        {
            $data = array(
               'business_id' => $business_id ,
               'university_id' => $university_id ,
               'expiration' => $expiration 
            );

            $this->db->where(self::primary_key,$id);
            $this->db->update($table,$data);

        }
    }

    function delete_entry($id)
    {
        $table = self::table;
        if($id == null)
        {
            return false;
        }
        else
        {
            $this->db->where(self::primary_key,$id);
            $this->db->delete($table);

        }
    }



} 

?>