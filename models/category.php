<?php
class Category extends CI_Model
{
    const table = 'categories';
    const primary_key = 'category_id';
    const search_field = 'category_name';

    const default_order = "asc";


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

   
    function get_all_entries()
    {
        $table = self::table;
        $this->db->order_by(self::search_field,self::default_order);

        //Query the data table for every record and row  
  		$query = $this->db->get($table);
        if ($query->num_rows() > 0)  
        {  
             return $query->result();
        }
        else
        {  
              return false;
        }
       
    }
    
    function get_entry_by_id($id = null)
    { 
        $table = self::table;
        if($id == null)
        {
            return false;
        }
        else
        {
            $this->db->from($table)->where(self::primary_key,$id)->limit(1);

            $query = $this->db->get();
            if($query->num_rows() > 0)
            {
                return $query->result();
            }   
            else
            {
                return false;
            }
        }
    }
    
    function get_entries_by_name($name = '')
    {
        $table = self::table;

        $query = $this->db->from($table)->like(self::search_field,$name);
             
        if($query->num_rows() > 0)
        {
            return $query->result();
        }   
        else
        {
            return false;
        }
        
    }
	
    function get_model_name()
    {
        return "Category";
    }

    function insert_entry($category_name,$description)
    {
        $table = self::table;

        $data = array(
           'category_name' => $category_name ,
           'description' => $description
        );

        $this->db->insert($table,$data);

        
    }

    function update_entry($id,$category_name,$description)
    {
        $table = self::table;
        if($id == null)
        {
            return false;
        }
        else
        {
            $data = array(
               'category_name' => $category_name ,
               'description' => $description
            );

            $this->db->where(self::primary_key,$id);
            $this->db->update($table,$data);

        }
    }

    function delete_entry($id)
    {
        $table = self::table;
        if($id == null)
        {
            return false;
        }
        else
        {
            $this->db->where(self::primary_key,$id);
            $this->db->delete($table);

        }
    }
} 

?>
