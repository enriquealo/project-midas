<?php
class Branch extends CI_Model
{
    const table = 'branches';
    const primary_key = 'branch_id';
    const search_field = 'branch_name';

    const default_order = "asc";


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_all_entries()
    {
        $table = self::table
        $this->db->order_by(self::search_field,self::default_order);
 
        $query = $this->db->get($table);
        if ($query->num_rows() > 0)  
        {  
             return $query->result();
        }
        else
        {  
              return false;
        }
       
    }
    
    function get_entry_by_id($id = null)
    { 
        $table = self::table
        if($id == null)
        {
            return false;
        }
        else
        {
            $this->db->from($table)->where(self::primary_key,$id)->limit(1);

            $query = $this->db->get();
            if($query->num_rows() > 0)
            {
                return $query->result();
            }   
            else
            {
                return false;
            }
        }
    }
    
    function get_entries_by_name($name = '')
    {
        $table = self::table

        $query = $this->db->from($table)->like(self::search_field,$name);
             
        if($query->num_rows() > 0)
        {
            return $query->result();
        }   
        else
        {
            return false;
        }
        
    }
    
    function get_model_name()
    {
        return "universities";
    }

    function get_entries_by_country($country_id)
    {
        $table = self::table
        
        if($country_id == null)
        {
            return false;
        }
        else
        {
            $this->db->from($table)->join('countries','countries.country_id = {$table}.country_id')->where('country_id',$country_id);
            $query = $this->db->get();
             
            if($query->num_rows() > 0)
            {
                return $query->result();
            }   
            else
            {
                return false;
            }
        }
    }

    function get_entries_by_business($business_id){
        $table = self::table
        
        if($business_id == null)
        {
            return false;
        }
        else
        {
            $this->db->from($table)->join('businesses','businesses.business_id = {$table}.business_id')->where('business_id',$business_id);
            $query = $this->db->get();
             
            if($query->num_rows() > 0)
            {
                return $query->result();
            }   
            else
            {
                return false;
            }
        }
    }

    function insert_entry($name,$address,$phone,$country_id,$business_id)
    {
        $table = self::table

        $data = array(
           'name' => $name ,
           'address' => $address ,
           'phone' => $phone ,
           'country_id' => $country_id ,
           'business_id' => $business_id ,
        );

        $this->db->insert($table,$data);

        
    }

    function update_entry($id,$name,$address,$phone,$country_id,$business_id)
    {
        $table = self::table
        if($id == null)
        {
            return false;
        }
        else
        {
            $data = array(
               'name' => $name ,
               'address' => $address ,
               'phone' => $phone ,
               'country_id' => $country_id ,
               'business_id' => $business_id ,
            );

            $this->db->where(self::primary_key,$id);
            $this->db->update($table,$data);

        }
    }

    function delete_entry($id)
    {
        $table = self::table
        if($id == null)
        {
            return false;
        }
        else
        {
            $this->db->where(self::primary_key,$id);
            $this->db->delete($table);

        }
    }
} 

?>
