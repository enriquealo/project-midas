<?php
class Promo extends CI_Model
{
    const table = 'promos';
    const primary_key = 'promo_id';


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_all_entries()
    {
        $table = self::table;
 
        $query = $this->db->get($table);
        if ($query->num_rows() > 0)  
        {  
             return $query->result();
        }
        else
        {  
              return false;
        }
       
    }
    
    function get_entry_by_id($id = null)
    { 
        $table = self::table;
        if($id == null)
        {
            return false;
        }
        else
        {
            $this->db->from($table)->where(self::primary_key,$id)->limit(1);

            $query = $this->db->get();
            if($query->num_rows() > 0)
            {
                return $query->result();
            }   
            else
            {
                return false;
            }
        }
    }
    
    function get_model_name()
    {
        return "Promo";
    }

    function get_entries_by_branch($branch_id){
        $table = self::table;
        
        if($branch_id == null)
        {
            return false;
        }
        else
        {
            $this->db->from($table)->join('branches','branches.branch_id = {$table}.branch_id')->where('branch_id',$branch_id);
            $query = $this->db->get();
             
            if($query->num_rows() > 0)
            {
                return $query->result();
            }   
            else
            {
                return false;
            }
        }
    }

    function get_entries_by_business($business_id){
        $table = self::table;
        
        if($business_id == null)
        {
            return false;
        }
        else
        {
            $this->db->from($table)->join('branches','branches.branch_id = {$table}.branch_id');
            $this->db->join('businesses','businesses.business_id = branches.business_id');
            $this->db->where('business_id',$business_id);
            $query = $this->db->get();
             
            if($query->num_rows() > 0)
            {
                return $query->result();
            }   
            else
            {
                return false;
            }
        }
    }

    function get_entries_by_branch_and_agreement($branch_id,$agreement_id){
        $table = self::table;
        
        if($branch_id == null||$agreement_id == null)
        {
            return false;
        }
        else
        {
            $this->db->from($table)->join('branches','branches.branch_id = {$table}.branch_id');
            $this->db->join('businesses','businesses.business_id = branches.business_id');
            $this->db->join('agreements','businesses.business_id = agreements.business_id')
            $this->db->where('agreement_id',$agreement_id);
            $this->db->where('branch_id',$branch_id);

            $query = $this->db->get();
             
            if($query->num_rows() > 0)
            {
                return $query->result();
            }   
            else
            {
                return false;
            }
        }
    }

    function insert_entry($branch_id,$image,$description)
    {
        $table = self::table;

        $data = array(
           'branch_id' => $branch_id ,
           'image' => $image ,
           'description' => $description ,
        );

        $this->db->insert($table,$data);

        
    }

    function update_entry($id,$branch_id,$image,$description)
    {
        $table = self::table;
        if($id == null)
        {
            return false;
        }
        else
        {
            $data = array(
               'branch_id' => $branch_id ,
               'image' => $image ,
               'description' => $description ,
            );

            $this->db->where(self::primary_key,$id);
            $this->db->update($table,$data);

        }
    }

    function delete_entry($id)
    {
        $table = self::table;
        if($id == null)
        {
            return false;
        }
        else
        {
            $this->db->where(self::primary_key,$id);
            $this->db->delete($table);

        }
    }
} 

?>