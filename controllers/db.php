<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db extends CI_Controller {

	function __construct() {
		parent::__construct();
 
        // Load helpers
        $this->load->helper('url');
             
	}

	/**
	 * Index Page for this controller.
	 */
	public function index()
	{
		echo "Try using: object/parameter";
	}

	public function agreement(){
		$this->load->model('Agreement', 'agreement');

		$parameter = $this->uri->segment(3);
		if($parameter == null)
		{
			$agreement_list = $this->agreement->get_all_entries();
		}else if(is_numeric($parameter)){
			$agreement_list = $this->agreement->get_entry_by_id($parameter);
		}else{
			$agreement_list = $this->agreement->get_entries_by_name($parameter);
		}
		

		$this->_display_json($agreement_list);
	}

	public function branch(){
		$this->load->model('Branch', 'branch');

		$parameter = $this->uri->segment(3);
		if($parameter == null)
		{
			$branch_list = $this->branch->get_all_entries();
		}else if(is_numeric($parameter)){
			$branch_list = $this->branch->get_entry_by_id($parameter);
		}else{
			$branch_list = $this->branch->get_entries_by_name($parameter);
		}
		

		$this->_display_json($branch_list);
	}

	public function business(){
		$this->load->model('Business', 'business');

		$parameter = $this->uri->segment(3);
		if($parameter == null)
		{
			$business_list = $this->business->get_all_entries();
		}else if(is_numeric($parameter)){
			$business_list = $this->business->get_entry_by_id($parameter);
		}else{
			$business_list = $this->business->get_entries_by_name($parameter);
		}
		

		$this->_display_json($business_list);
	}

	public function category(){
		$this->load->model('Category', 'category');

		$parameter = $this->uri->segment(3);
		if($parameter == null)
		{
			$category_list = $this->category->get_all_entries();
		}else if(is_numeric($parameter)){
			$category_list = $this->category->get_entry_by_id($parameter);
		}else{
			$category_list = $this->category->get_entries_by_name($parameter);
		}
		

		$this->_display_json($category_list);
	}

	public function country(){
		$this->load->model('Country', 'country');

		$parameter = $this->uri->segment(3);
		if($parameter == null)
		{
			$country_list = $this->country->get_all_entries();
		}else if(is_numeric($parameter)){
			$country_list = $this->country->get_entry_by_id($parameter);
		}else{
			$country_list = $this->country->get_entries_by_name($parameter);
		}
		

		$this->_display_json($country_list);
	}

	public function promo(){
		$this->load->model('Promo', 'promo');

		$parameter = $this->uri->segment(3);
		if($parameter == null)
		{
			$promo_list = $this->promo->get_all_entries();
		}else if(is_numeric($parameter)){
			$promo_list = $this->promo->get_entry_by_id($parameter);
		}else{
			$promo_list = $this->promo->get_entries_by_name($parameter);
		}
		

		$this->_display_json($promo_list);
	}

	public function university(){
		$this->load->model('University', 'university');

		$parameter = $this->uri->segment(3);
		if($parameter == null)
		{
			$university_list = $this->university->get_all_entries();
		}else if(is_numeric($parameter)){
			$university_list = $this->university->get_entry_by_id($parameter);
		}else{
			$university_list = $this->university->get_entries_by_name($parameter);
		}
		

		$this->_display_json($university_list);
	}

	public function _display_json($data){
		if($data)
		{
			$this->output
	    		->set_content_type('application/json')
	    		->set_output(json_encode($data));
	    }
	}
}

/* End of file db.php */
/* Location: ./application/controllers/db.php */